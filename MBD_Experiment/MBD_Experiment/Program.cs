﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MBD_Experiment
{
    class Program
    {
        static uint f = 11 << 16; //Should be (1011----------------) NOTE 1,2,3,4 are used, but in exercise are 2,4,6,8
        static uint inverseF = Convert.ToUInt32("01000000000000000000", 2);
        // At index 0, this suggests x_i = 0 is still valid if position i is a 1.
        static uint[] h = new uint[] { Convert.ToUInt32("11111111111111111111", 2), Convert.ToUInt32("11111111111111111111", 2) };
        static uint mLimit = 0;
        static Random r = new Random();
        static bool wasTherePositive = false;
        static int amountEqualHF = 0;
        static int amountLossLess = 0;
        static double summedTrueLoss = 0;
        static double constantAmount = 1.0 / (1 << 4);

        static void Main()
        {
            Console.WriteLine("mLimit | amountLossLess | summedTrueLoss | amountEqualHF");
            while (mLimit < 486)
            {
                mLimit += 50;
                if (mLimit > 486) mLimit = 486;
                //Reset bookkeeping
                amountEqualHF = 0;
                amountLossLess = 0;
                summedTrueLoss = 0;
                for (int n = 0; n < 10000; n++)
                {
                    // Reset experiment
                    wasTherePositive = false;
                    h[0] = Convert.ToUInt32("11111111111111111111", 2);
                    h[1] = Convert.ToUInt32("11111111111111111111", 2);
                    for (int m = 0; m < mLimit; m++)
                    {
                        uint sample = exsample();
                        //Check if sample positive
                        //Console.WriteLine(((f ^ ~sample) >> 16) % 16);
                        if (((f ^ ~sample) >> 16) % 16 == 15)
                        {
                            wasTherePositive = true;
                            //Adjust h according to the sample
                            for (int t = 0; t < 20; t++)
                            {
                                uint index = 1 - ((sample >> t) & 1);
                                h[index] = h[index] & (uint)~(1 << t);
                            }
                        }
                    }
                    if (!wasTherePositive) n--;
                    else
                    {
                        //Update bookkeeping
                        //Compute true loss
                        int ks = countKs();
                        double loss = 0;
                        if (ks == 0) { amountEqualHF++; } // 0 ks means h is perfectly the same as f
                        else { loss = constantAmount * (1 - (1.0 / (1 << ks))); } // Otherwise we can calculate the true loss
                        summedTrueLoss += loss;
                        if (loss <= 0.05) amountLossLess++;
                        //Extra test for insurance
                        if (h[1] == f && h[0] == inverseF && loss != 0) throw new Exception("Oops, ks is not zero while h = f");
                    }
                }
                Console.WriteLine(mLimit + " " + amountLossLess + " " + summedTrueLoss + " " + amountEqualHF);
            }
            Console.ReadLine();
        }

        static uint exsample()
        {
            return (uint)r.Next(1048575 + 1); //Returns a bitsting with some of the first 20 bits at one and some at zero
        }

        static int countKs()
        {
            int counter = 0;
            for (int t = 0; t < 2; t++)
            {
                for (int i=0; i < 16; i++)
                {
                    counter += ((h[t] >> i) & 1) == 1 ? 1 : 0; //The ? : is needed to stop the compiler from complaining
                }
            }
            return counter;
        }
    }
}
